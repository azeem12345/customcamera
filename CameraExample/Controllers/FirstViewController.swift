//
//  FirstViewController.swift
//  CameraExample
//
//  Created by Azeem Ahmed on 6/10/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation
import CoreMotion

import JQSwiftIcon

import Photos

import ScalePicker
import CariocaMenu
import FirebaseCrashlytics

enum SettingMenuTypes {
    case none, cameraSliderMenu, resolutionMenu, flashMenu, allStatsMenu, miscMenu
}

class Singleton {

    //MARK: Shared Instance

    static let sharedInstance : Singleton = {
        let instance = Singleton()
        return instance
    }()

    //MARK: Local Variable

    var emptyStringArray : [String]? = nil

    //MARK: Init

    convenience init() {
        self.init(array : [])
    }

    //MARK: Init Array

    init( array : [String]) {
        emptyStringArray = array
    }
}

class FirstViewController:
    UIViewController,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    UIGestureRecognizerDelegate,
    CariocaMenuDelegate {

    let VIDEO_RECORD_INTERVAL_COUNTDOWN:        Double = 1

    @IBOutlet var myCamView:                    UIView!
    @IBOutlet var doPhotoBtn:                   UIButton!
    @IBOutlet var doVideoBtn:                   UIButton!
    @IBOutlet var actionToolbar:                UIToolbar!
    @IBOutlet var menuHostView:                 MenuHostView!
    @IBOutlet var FPSLabel:                     UILabel!
    @IBOutlet var videoCounterLabel:            UILabel!
    @IBOutlet var videoRecordIndicator:         UIImageView!
    @IBOutlet var resolutionHostBlurView:       SharedBlurView!
    @IBOutlet var enablePermsView:              SharedBlurView!
    @IBOutlet var gridHostView:                 UIView!
    @IBOutlet var lblISO:                       UILabel!
    @IBOutlet var btnZoomView:                  UIView!
    @IBOutlet var btnPhotoCaptureView:          UIView!
    @IBOutlet var innerLayerViewForCropping:    UIView!
    @IBOutlet var makingPhotoView:    UIView!
    @IBOutlet var tfISO:    UITextField!
    @IBOutlet var tfApeture:    UITextField!
    @IBOutlet var tfShutter:    UITextField!
    @IBOutlet var tfExposureValue4:    UITextField!
    @IBOutlet var tfQualityofImage:    UITextField!
    @IBOutlet var tfCommon:    UITextField!
    @IBOutlet var hdrValueSetView:    UIView!
    @IBOutlet weak var exposureSlider1: GradientSlider!
    @IBOutlet weak var exposureSlider2: GradientSlider!
    @IBOutlet weak var exposureSlider3: GradientSlider!
    @IBOutlet weak var exposureSlider4: GradientSlider!
    @IBOutlet weak var imageQualitySlider5: GradientSlider!
    @IBOutlet var lblExposureValue1:    UILabel!
    @IBOutlet var lblExposureValue2:    UILabel!
    @IBOutlet var lblExposureValue3:    UILabel!
    @IBOutlet var lblExposureValue4:    UILabel!
    @IBOutlet var lblQualityofImage:    UILabel!
    @IBOutlet var layerViewForPC:    UIView!
    @IBOutlet var lblProcessing:    UILabel!
    
    @IBOutlet var tfContrast:    UITextField!
    @IBOutlet var tfSaturation:    UITextField!
    @IBOutlet var tfExposure:    UITextField!
    
    
    let motionManager = CMMotionManager()
    var timer: Timer!
    var timerForTilted: Timer!
    var motionValue = ""
    
    //For Tilted
    var xValue = 0.0
    var yValue = 0.0
    @IBOutlet weak var customView: UIView!
    var originX : Int = 0 , originY : Int = 0, heightCV : Int = 0 , widthCV : Int = 0
    var widthC: CGFloat = 0
    var orientationsPosition = "portrait"
    @IBOutlet weak var customViewHeightCons: NSLayoutConstraint!
    @IBOutlet weak var customViewwidthCons: NSLayoutConstraint!
    
    @IBOutlet weak var makingPhotoViewHeightCons: NSLayoutConstraint!
    @IBOutlet weak var makingPhotoViewwidthCons: NSLayoutConstraint!
    
    @IBOutlet weak var myCamViewViewWidthCons: NSLayoutConstraint!
    @IBOutlet weak var myCamViewViewHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var layerViewWidthCons: NSLayoutConstraint!
    @IBOutlet weak var layerViewHeightCons: NSLayoutConstraint!
    
    var oldShapLayer = CAShapeLayer()
    var overLayShape = CAShapeLayer()

    var captureSessionManager:                  CaptureSessionManager! = CaptureSessionManager.sharedInstance

    var logging:                                Bool = true

    private var videoRecordCountdownSeconds:    Double = 0.0
    private var videRecordCountdownTimer:       Timer!

    private var optionsMenu:                    CariocaMenu?
    private var cariocaMenuViewController:      CameraMenuContentController?

    //menu controllers here
    private var cameraOptionsViewController:    CameraOptionsViewController?
    private var cameraSecondaryOptions:         RightMenuSetViewController?
    private var cameraResolutionSideMenu:       ResolutionSideMenuViewController?
    private var cameraResolutionMenu:           ResolutionViewController?

    private var focusZoomView:                  FocusZoomViewController?

    private var gridManager:                    GridManager!
    
    var hdrFlag: Bool = false

    //flag that determines if a user gave all required perms: photo library, video, microphone
    private var isAppUsable:                    Bool = false
    public var isPhotoOnly:                    Bool = false
    
    var rotationAngle: Double = 0.0
    var portraitTopWidth = 0
    var horizontalDegree : Double = 150.0

    override func viewDidLoad() {
        super.viewDidLoad()
        //layerViewForPC = myCamView
        self.myCamViewViewHeightCons.constant = 550//self.view.frame.height
        self.myCamViewViewWidthCons.constant = self.view.frame.width
        
        layerViewHeightCons.constant = self.myCamViewViewHeightCons.constant
        layerViewWidthCons.constant = self.myCamViewViewWidthCons.constant

        //myCamView.backgroundColor = .red
        //layerViewForPC.backgroundColor = .yellow
        //makingPhotoView.backgroundColor = .green
        
        makingPhotoViewHeightCons.constant = self.myCamViewViewHeightCons.constant
        makingPhotoViewwidthCons.constant = self.myCamViewViewWidthCons.constant
        
        addCoreObservers()
        processUi()
        self.setPositioningOfElement(zPositionValue: 1)
        self.innerLayerViewForCropping.isHidden = true
        customViewHeightCons.constant = 0
        customViewwidthCons.constant = 0
        
        innerLayerViewForCropping.layer.borderColor = UIColor.black.cgColor
        innerLayerViewForCropping.layer.borderWidth = 1
        hdrValueSetView.layer.zPosition = 1
        makingPhotoView.clipsToBounds = false
        
        setHRDSlider()
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
//        self.view.addGestureRecognizer(tapGesture)
        
    }
    
//    @objc func tap(gesture: UITapGestureRecognizer) {
//        if(tfCommon != nil){
//            tfCommon.resignFirstResponder()
//        }
//    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        captureSessionManager.firstViewController = self
        setNeedsStatusBarAppearanceUpdate()
    }

    override func didReceiveMemoryWarning() {
        print("[didReceiveMemoryWarning] memory warning happened.")
        super.didReceiveMemoryWarning()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //layerViewForPC.frame = self.view.frame
        if (optionsMenu?.hostView == nil) {
            optionsMenu?.addInView(self.view)
        }

        gridManager = GridManager.init(gridView: gridHostView, storyBoard: self.storyboard!, parentViewDimensions: gridHostView.bounds)
        gridHostView.layer.zPosition = 1
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }  

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func setPositioningOfElement(zPositionValue: CGFloat){
        btnPhotoCaptureView.layer.zPosition = zPositionValue
        btnZoomView.layer.zPosition = zPositionValue
        self.menuHostView.layer.zPosition = zPositionValue
        cameraSecondaryOptions?.view.layer.zPosition = zPositionValue
        myCamView.layer.zPosition = zPositionValue
        makingPhotoView.layer.zPosition = zPositionValue
    }

    /////////////////// Carioca Menu Overrides START

    func cariocaMenuDidSelect(_ menu:CariocaMenu, indexPath:IndexPath) {
        cariocaMenuViewController?.menuWillClose()

        hideActiveSetting() {_ in
            print("Done hiding from show")
            self.setPositioningOfElement(zPositionValue: 1)

            //todo -> switchcase for misc menu
            self.menuHostView.setActiveMenu(self.cameraOptionsViewController!, menuType: .cameraSliderMenu)
            
            self.menuHostView.setCameraSliderViewControllerForIndex(indexPath.row, callbackToOpenMenu: self.showActiveSetting)
            self.optionsMenu?.moveToTop()
        }
    }

    func cariocaMenuWillOpen(_ menu:CariocaMenu) {
        cariocaMenuViewController?.menuWillOpen()
        if logging {
            self.setPositioningOfElement(zPositionValue: 0)
            print("carioca MenuWillOpen \(menu)")
        }
    }

    func cariocaMenuDidOpen(_ menu:CariocaMenu){
        if logging {
            switch menu.openingEdge{
            case .left:
                print("carioca MenuDidOpen \(menu) left")
                break;
            default:
                print("carioca MenuDidOpen \(menu) right")
                break;
            }
        }
    }

    func cariocaMenuWillClose(_ menu:CariocaMenu) {
        cariocaMenuViewController?.menuWillClose()
        if logging {
            print("carioca MenuWillClose \(menu)")
        }
    }

    func cariocaMenuDidClose(_ menu:CariocaMenu){
        if logging {
            print("carioca MenuDidClose \(menu)")
        }
    }
    /////////////////// Carioca Menu Overrides END

    @IBAction func onDoPhotoTrigger(_ sender: AnyObject) {
        if(self.innerLayerViewForCropping.isHidden){
            captureImage()
        }else{
            captureSessionManager.setPreviewLayerOrientation(.portrait)
            captureImage()
            //let img =  UIImage.init(view: myCamView)
            //self.getCaptureImage(image: img)
        }
    }

    @IBAction func onDoVideo(_ sender: UIButton) {
        startStopRecording()
    }

    @objc func captureImage() {
        if isAppUsable {
            if cameraSecondaryOptions?.timerScale != TimerScales.off  {
                if (cameraSecondaryOptions?.timerState != TimerStates.ticking) {
                    doPhotoBtn.isEnabled = false
                    doPhotoBtn.alpha = 0.4
                    cameraSecondaryOptions?.startTimerTick {
                        self.doPhotoBtn.isEnabled = true
                        self.doPhotoBtn.alpha = 1
                        self.captureSessionManager.captureImage()
                    }
                }
            } else {
                doPhotoBtn.alpha = 1
                doPhotoBtn.isEnabled = true
                self.captureSessionManager.captureImage()
            }
        }
    }

    @objc func startStopRecording() {
        if isAppUsable && !isPhotoOnly {
            self.captureSessionManager.startStopRecording()
        }
    }

    @objc func applicationDidEnterBackground() {
        print("[applicationDidEnterBackground] start")
        onDispose()
    }
    
    @IBAction func btnResetExposureValue(_ sender: UIButton) {
        exposureSlider1.value = -2
        exposureSlider2.value = -1
        exposureSlider3.value = 2
        exposureSlider4.value = 3
        imageQualitySlider5.value = 0.5
        lblExposureValue1.text = "-2"
        lblExposureValue2.text = "-1"
        lblExposureValue3.text = "2"
        lblExposureValue4.text = "3"
        lblQualityofImage.text = "0.5"
        tfISO.text = "-2"
        tfApeture.text = "-1"
        tfShutter.text = "2"
        tfExposureValue4.text = "3"
        tfQualityofImage.text = "0.5"
        tfContrast.text = "0"
        tfSaturation.text = "0"
        tfExposure.text = "0.02"
    }


    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        if isAppUsable {
            var point: CGPoint = gestureRecognizer.location(in: gestureRecognizer.view)

            if (gestureRecognizer.state == .began) {
                if(self.focusZoomView == nil) {
                    self.focusZoomView = self.storyboard?.instantiateViewController(withIdentifier: "FocusZoomView") as? FocusZoomViewController
                }

                self.focusZoomView?.resetView()

                gestureRecognizer.view?.addSubview((self.focusZoomView?.view)!)

                self.focusZoomView?.view.transform = CGAffineTransform.init(translationX: point.x - (focusZoomView?.view.bounds.width)!/2, y: point.y - (focusZoomView?.view.bounds.height)!/2)

                self.focusZoomView?.appear()
            }

            if (gestureRecognizer.state == .changed) {
                self.focusZoomView?.view.transform = CGAffineTransform.init(translationX: point.x - (focusZoomView?.view.bounds.width)!/2, y: point.y - (focusZoomView?.view.bounds.height)!/2)
            }

            if (gestureRecognizer.state == .ended) {
                let centerDelta: CGFloat = 100.0
                if (point.x <= (gestureRecognizer.view?.bounds.width)!/2 + centerDelta &&
                    point.x >= (gestureRecognizer.view?.bounds.width)!/2 - centerDelta &&
                    point.y <= (gestureRecognizer.view?.bounds.height)!/2 + centerDelta &&
                    point.y >= (gestureRecognizer.view?.bounds.height)!/2 - centerDelta ) {

                    point = CGPoint.init(x: (gestureRecognizer.view?.bounds.width)!/2, y: (gestureRecognizer.view?.bounds.height)!/2)

                    self.focusZoomView?.disolveToRemove()
                } else {
                    self.focusZoomView?.disolve()
                }

                self.focusZoomView?.view.transform = CGAffineTransform.init(translationX: point.x - (focusZoomView?.view.bounds.width)!/2, y: point.y - (focusZoomView?.view.bounds.height)!/2)

                self.captureSessionManager.setPointOfInterest(point)
            }
        }
    }
    
    @IBAction func zoomInZoomOut(_ sender: UIButton) {
        self.captureSessionManager.zoomScale = CGFloat(0.5)
        let btn = sender
        var zoomScale: CGFloat = 1.0
        if(btn.tag == 0){
            zoomScale = 0.5
        }else if(btn.tag == 1){
            zoomScale = 1.0
        }else if(btn.tag == 2){
            zoomScale = 2.0
        }else if(btn.tag == 3){
            zoomScale = 3.0
        }
        self.captureSessionManager._zoom(zoomScale)
    }

    @objc func handlerCamViewTap(_ gestureRecognizer: UIGestureRecognizer) {
        if isAppUsable {
            if (menuHostView != nil && menuHostView.activeMenuType != .none) {
                if (menuHostView.activeMenuType == .cameraSliderMenu) {
                    self.toggleCariocaIndicatorPin()
                }
                hideActiveSetting() {_ in
                    print("Done hiding from tap")
                }
            } else {
                if(self.focusZoomView == nil) {
                    self.focusZoomView = self.storyboard?.instantiateViewController(withIdentifier: "FocusZoomView") as? FocusZoomViewController
                }

                self.focusZoomView?.resetView()

                gestureRecognizer.view?.addSubview((self.focusZoomView?.view)!)
                var point: CGPoint = gestureRecognizer.location(in: gestureRecognizer.view)
                let centerDelta: CGFloat = 100.0
                if (point.x <= (gestureRecognizer.view?.bounds.width)!/2 + centerDelta &&
                    point.x >= (gestureRecognizer.view?.bounds.width)!/2 - centerDelta &&
                    point.y <= (gestureRecognizer.view?.bounds.height)!/2 + centerDelta &&
                    point.y >= (gestureRecognizer.view?.bounds.height)!/2 - centerDelta ) {

                    point = CGPoint.init(x: (gestureRecognizer.view?.bounds.width)!/2, y: (gestureRecognizer.view?.bounds.height)!/2)

                    self.focusZoomView?.disolveToRemove()
                } else {
                    self.focusZoomView?.disolve()
                }

                self.focusZoomView?.view.transform = CGAffineTransform.init(translationX: point.x - (focusZoomView?.view.bounds.width)!/2, y: point.y - (focusZoomView?.view.bounds.height)!/2)

                self.captureSessionManager.setPointOfInterest(point)
            }
        }
    }

    open func onShowResOptions() {
        if isAppUsable {
            if (menuHostView.activeMenuType != .resolutionMenu) {
                if (menuHostView.activeMenuType == .cameraSliderMenu) {
                    self.toggleCariocaIndicatorPin()
                }
                
                hideActiveSetting() { _ in
                    if(self.cameraResolutionMenu == nil) {
                        self.cameraResolutionMenu = self.storyboard?.instantiateViewController(withIdentifier: "CameraResolutionMenu") as? ResolutionViewController
                    }
                    
                    self.menuHostView.setActiveMenu(self.cameraResolutionMenu!, menuType: .resolutionMenu)
                    
                    self.showActiveSetting()
                }
            }
        }
    }
    
    private func getIsAppUsable(isVideoEnabled: Bool, isAudioEnabled: Bool, isPhotoLibraryEnabled: Bool) -> Bool {
        let isPhotoAndVideo = isVideoEnabled && isAudioEnabled && isPhotoLibraryEnabled
        isPhotoOnly = isVideoEnabled && !isAudioEnabled && isPhotoLibraryEnabled
        
        return isPhotoAndVideo || isPhotoOnly
    }

    @objc func requestPhotoVideoAudioPerms() {
        //let videoAuthState      = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        //let audioAuthState      = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        let libraryAuthState    = PHPhotoLibrary.authorizationStatus()
        //var isVideoEnabled          = videoAuthState ==  AVAuthorizationStatus.authorized
        //var isAudioEnabled          = audioAuthState ==  AVAuthorizationStatus.authorized
        var isPhotoLibraryEnabled   = libraryAuthState == PHAuthorizationStatus.authorized

//        if  !isAudioEnabled {
//            AVCaptureDevice.requestAccess(for: AVMediaType.audio, completionHandler: { (granted :Bool) -> Void in
//                isAudioEnabled = granted
//                if !isVideoEnabled {
//                    AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
//                        isVideoEnabled = granted
//
//                        if (!isPhotoLibraryEnabled) {
//                            PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
//                                isPhotoLibraryEnabled = authorizationStatus == PHAuthorizationStatus.authorized
//                                self.isAppUsable = self.getIsAppUsable(isVideoEnabled: isVideoEnabled, isAudioEnabled: isAudioEnabled, isPhotoLibraryEnabled: isPhotoLibraryEnabled)
//                            })
//                        }
//                    });
//                }
//            });
//        }
        
        if (!isPhotoLibraryEnabled) {
                                   PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                                       isPhotoLibraryEnabled = authorizationStatus == PHAuthorizationStatus.authorized
                                       self.isAppUsable = self.getIsAppUsable(isVideoEnabled: true, isAudioEnabled: false, isPhotoLibraryEnabled: isPhotoLibraryEnabled)
                                   })
        }
        
        
        
//isAudioEnabled
        //isVideoEnabled
        isAppUsable = self.getIsAppUsable(isVideoEnabled: true, isAudioEnabled: false, isPhotoLibraryEnabled: isPhotoLibraryEnabled)
        if (isAppUsable) {
            self.captureSessionManager.resetCaptureSession(camView: myCamView, isPhotoOnly: isPhotoOnly)
            enableUi()
        } else {
            if (libraryAuthState == PHAuthorizationStatus.denied) {
                 showAlert(title: "Custom Camera", message: "Open Settings and allow permission of camera")
            }
//            let areAnyStatesNotDetermined = videoAuthState == AVAuthorizationStatus.notDetermined ||
//                audioAuthState == AVAuthorizationStatus.notDetermined ||
//                libraryAuthState == PHAuthorizationStatus.notDetermined
//            disableUi(areAnyStatesNotDetermined)
        }
    }
    
    
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)

        let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
            // Take the user to Settings app to possibly change permission.
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        // Finished opening URL
                    })
                } else {
                    // Fallback on earlier versions
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        })
        alert.addAction(settingsAction)

        self.present(alert, animated: true, completion: nil)
    }

    func startStopVideoCounter(start: Bool) {
        if start {
            //video countdown counter starts here
            
            UIView.animate(withDuration: self.VIDEO_RECORD_INTERVAL_COUNTDOWN/2, delay: 0, options: .curveEaseOut, animations: {
                self.videoRecordIndicator.alpha = 0.5
                self.videoCounterLabel.alpha = 1.0
                self.videoCounterLabel.text = String(format: "%02d:%02d:%02d", 0.0, 0.0, 0.0)
            }) { success in

                if (self.videRecordCountdownTimer != nil) {
                    self.videRecordCountdownTimer.invalidate()
                }

                self.videRecordCountdownTimer = Timer.scheduledTimer(withTimeInterval: self.VIDEO_RECORD_INTERVAL_COUNTDOWN/2, repeats: true, block: {timer in
                    let videoRecordCountdownSeconds = (self.captureSessionManager.captureVideoOut?.recordedDuration.seconds)!

                    let seconds: Int = Int(videoRecordCountdownSeconds) % 60
                    let minutes: Int = Int((videoRecordCountdownSeconds / 60)) % 60
                    let hours: Int = Int(videoRecordCountdownSeconds) / 3600

                    DispatchQueue.main.async {
                        self.videoCounterLabel.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
                    }

                    UIView.animate(withDuration: self.VIDEO_RECORD_INTERVAL_COUNTDOWN/3, delay: 0, options: .curveEaseOut, animations: {
                        self.videoRecordIndicator.alpha = self.videoRecordIndicator.alpha == 0.5 ? 0.1 : 0.5
                    })
                })
            }
        } else {
                videRecordCountdownTimer.invalidate()
                videRecordCountdownTimer = nil

                UIView.animate(withDuration: self.VIDEO_RECORD_INTERVAL_COUNTDOWN/2, delay: 0, options: .curveEaseOut, animations: {
                    self.videoRecordIndicator.alpha = 0.0
                    self.videoRecordCountdownSeconds = 0.0
                    self.videoCounterLabel.alpha = 0.0

                }) { (success:Bool) in
                    DispatchQueue.main.async {
                    self.videoCounterLabel.text = String()
                }
            }
        }
    }

    private func onDispose(dealocateViews: Bool = true) {
        print("[onDispose] disposing")
        
        self.optionsMenu?.hideMenu()
        
        self.captureSessionManager.onSessionDispose()
        
        //cuz zoomView has a bounce timer
        //and order here MATTERS. MUST be before
        //the for loop bellow
        self.focusZoomView?.immediateReset()
        
        //must ALWAY go last. Release all other resources before
        //this for loop
        
        if dealocateViews, let layers = myCamView.layer.sublayers as [CALayer]? {
            for layer in layers  {
                layer.removeFromSuperlayer()
            }
        }
    }

    private func addCoreObservers() {

        //each time you spawn application back -> this observer gonna be triggered
        NotificationCenter.default.addObserver(self, selector: #selector(FirstViewController.requestPhotoVideoAudioPerms), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FirstViewController.applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)

        captureSessionManager.cameraSettingsObservable.subscribe(onNext: { (newCameraSettings: CameraSessionSettings) in
            let isFlashAvailable = newCameraSettings.isFlashAvailable

            if self.cameraSecondaryOptions != nil {
                if  isFlashAvailable &&
                    self.captureSessionManager.recodringState != RecordingStates.on {

                    self.cameraSecondaryOptions?.isFlashAvailable = true
                    self.captureSessionManager.flashModeState = (self.cameraSecondaryOptions?.flashModeState)!
                } else {
                    self.cameraSecondaryOptions?.isFlashAvailable = false
                    self.captureSessionManager.flashModeState = AVCaptureDevice.FlashMode.off
                }

                let recordingState = newCameraSettings.recordingState
                switch recordingState {
                    case RecordingStates.on:
                        self.doVideoBtn.titleLabel?.textColor = UIColor.red

                        self.cameraSecondaryOptions?.isOrientationSwitchEnabled = false
                        self.cameraSecondaryOptions?.isFlashAvailable = false
                        self.captureSessionManager.flashModeState = AVCaptureDevice.FlashMode.off

                        if (self.videRecordCountdownTimer == nil) {
                            self.startStopVideoCounter(start: true)
                        }

                    case RecordingStates.off:
                        self.doVideoBtn.titleLabel?.textColor = UIColor.white

                        self.cameraSecondaryOptions?.isOrientationSwitchEnabled = true
                        self.cameraSecondaryOptions?.isFlashAvailable = true
                        self.captureSessionManager.flashModeState = (self.cameraSecondaryOptions?.flashModeState)!

                        if (self.videRecordCountdownTimer != nil) {
                            self.startStopVideoCounter(start: false)
                        }
                }
            }
        })
    }

    private func processUi() {
        //TODO: re-factor the method! it's too damn big
        
        let camViewTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(FirstViewController.handlerCamViewTap))
        camViewTapRecognizer.numberOfTapsRequired = 1
        camViewTapRecognizer.numberOfTouchesRequired = 1

//        let camViewDoubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(FirstViewController.captureImage))
//        camViewDoubleTapRecognizer.numberOfTapsRequired = 2
//        camViewDoubleTapRecognizer.numberOfTouchesRequired = 1

//        let camViewTrippleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(FirstViewController.startStopRecording))
//
//        camViewTrippleTapRecognizer.numberOfTapsRequired = 3
//        camViewTrippleTapRecognizer.numberOfTouchesRequired = 1

        let camViewLongTapRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(FirstViewController.handleLongPress))

        self.myCamView.addGestureRecognizer(camViewTapRecognizer)
        //myCamView.addGestureRecognizer(camViewDoubleTapRecognizer)
        //myCamView.addGestureRecognizer(camViewTrippleTapRecognizer)
        self.myCamView.addGestureRecognizer(camViewLongTapRecognizer)

        //setting gesture priorities
        //camViewTapRecognizer.require(toFail: camViewDoubleTapRecognizer)
        //camViewTapRecognizer.require(toFail: camViewTrippleTapRecognizer)
        //camViewDoubleTapRecognizer.require(toFail: camViewTrippleTapRecognizer)

        //todo: all settings processing should be moved in to a single unit
        cameraOptionsViewController = self.storyboard?.instantiateViewController(withIdentifier: "CameraOptionsSlider") as? CameraOptionsViewController
        
        setupCameraSettingsSwipeMenu()

        cameraSecondaryOptions = self.storyboard?.instantiateViewController(withIdentifier: "RightMenuViewController") as? RightMenuSetViewController
        view.addSubview((cameraSecondaryOptions?.view)!)

        var viewHeight = view.bounds.height
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows.first
            if (window != nil) {
                viewHeight = window!.safeAreaLayoutGuide.layoutFrame.height
            }
        }
        
        cameraSecondaryOptions?.view.transform = CGAffineTransform.init(translationX: view.bounds.width-(cameraSecondaryOptions?.view.bounds.width)! + 5, y: viewHeight - (cameraSecondaryOptions?.view.bounds.height)! - 80)
        
        cameraResolutionSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "ResolutionSideMenuViewController") as? ResolutionSideMenuViewController
        
        //view.addSubview((cameraResolutionSideMenu?.view)!)

        cameraResolutionSideMenu?.view.transform = CGAffineTransform.init(translationX: -2, y: viewHeight - (cameraResolutionSideMenu?.view.bounds.height)! - 36)
        
        cameraResolutionSideMenu?.setTouchEndCb(cb: onShowResOptions)

        self.cameraSecondaryOptions?.addObserver(self, forKeyPath: "orientationRawState", options: NSKeyValueObservingOptions.new, context: nil)
        self.cameraSecondaryOptions?.addObserver(self, forKeyPath: "gridRawState", options: NSKeyValueObservingOptions.new, context: nil)
        self.cameraSecondaryOptions?.addObserver(self, forKeyPath: "flashModeRawState", options: NSKeyValueObservingOptions.new, context: nil)
        self.cameraSecondaryOptions?.addObserver(self, forKeyPath: "hdrRawState", options: NSKeyValueObservingOptions.new, context: nil)
        self.cameraSecondaryOptions?.addObserver(self, forKeyPath: "shakeRawState", options: NSKeyValueObservingOptions.new, context: nil)
        self.cameraSecondaryOptions?.addObserver(self, forKeyPath: "tiltedRawState", options: NSKeyValueObservingOptions.new, context: nil)
        

        doPhotoBtn.processIcons();
        doVideoBtn.processIcons();
    }

    private func enableUi() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.doPhotoBtn.isEnabled = true
            self.doPhotoBtn.alpha = 1
            
            if self.isPhotoOnly {
                self.disableVideoRecording()
            } else {
                self.doVideoBtn.isEnabled = true
                self.doVideoBtn.alpha = 1
            }
            
            self.cameraSecondaryOptions?.view.isHidden = false
            self.cameraResolutionSideMenu?.view.isHidden = false
            self.enablePermsView.isHidden = true

            self.toggleCariocaIndicatorPin()
            
            //todo -> reload UI method
            self.cameraResolutionSideMenu?.resModePicker.reloadComponent(0)
        })
    }
    
    private func toggleCariocaIndicatorPin(isEnabled: Bool = true) {
        
        if (self.optionsMenu?.hostView == nil) {
            optionsMenu?.addInView(self.view)
        }
        
        var yOffset: CGFloat = -50
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows.first
            if (window != nil) {
                yOffset = -(view.bounds.height - window!.safeAreaLayoutGuide.layoutFrame.height) - 30
            }
        }
        
        if(!isEnabled) {
            yOffset = 50
        }
        
        self.optionsMenu?.showIndicator(.right, position: .bottom, offset: yOffset)
    }

    private func disableVideoRecording(_ areAnyStatesNotDetermined: Bool = false) {
        doVideoBtn.isEnabled = false
        doVideoBtn.alpha = 0.4
    }
    
    private func disableUi(_ areAnyStatesNotDetermined: Bool = false) {
        doPhotoBtn.isEnabled = false
        doPhotoBtn.alpha = 0.4
        doVideoBtn.isEnabled = false
        doVideoBtn.alpha = 0.4
        cameraSecondaryOptions?.view.isHidden = true
        cameraResolutionSideMenu?.view.isHidden = true
        if (!areAnyStatesNotDetermined) {
            enablePermsView.isHidden = false
        }
        hideActiveSetting { (AnyObject) in
            print("done hiding")
        }
        
        self.toggleCariocaIndicatorPin(isEnabled: false)
    }

    private func setupCameraSettingsSwipeMenu() {
        cariocaMenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "CameraMenu") as? CameraMenuContentController
        //Set the tableviewcontroller for the shared carioca menu
        optionsMenu = CariocaMenu(dataSource: cariocaMenuViewController!)
        optionsMenu?.selectedIndexPath = IndexPath(item: 0, section: 0)

        optionsMenu?.delegate = self
        optionsMenu?.boomerang = .verticalAndHorizontal

        optionsMenu?.selectedIndexPath = IndexPath(row: (cariocaMenuViewController?.iconNames.count)! - 1, section: 0)

        //reverse delegate for cell selection by tap :
        cariocaMenuViewController?.cariocaMenu = optionsMenu
    }

    private func showActiveSetting() {
        if isAppUsable {
            menuHostView.center.x = self.view.center.x

            var tBefore = CGAffineTransform.identity
            tBefore = tBefore.translatedBy(x: 0, y: self.view.bounds.height/2 + self.menuHostView.bounds.height + self.actionToolbar.bounds.height)
            tBefore = tBefore.scaledBy(x: 0.6, y: 1)
            
            menuHostView.transform = tBefore
            
            menuHostView.isHidden = false

            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                var tAfter = CGAffineTransform.identity
                tAfter = tAfter.translatedBy(x: 0, y: self.view.bounds.height/2 - self.menuHostView.bounds.height - self.actionToolbar.bounds.height/2)
                tAfter = tAfter.scaledBy(x: 1, y: 1)
                
                self.menuHostView.transform = tAfter
            })
        }
    }

    private func hideActiveSetting(_ completion: @escaping (_ result: AnyObject) -> Void) {
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            
            var t = CGAffineTransform.identity
            t = t.translatedBy(x: 0, y: self.view.bounds.height/2 + self.menuHostView.bounds.height + self.actionToolbar.bounds.height)
            t = t.scaledBy(x: 1.4, y: 1)
            
            self.menuHostView.transform = t
        }) { (success:Bool) in
            self.menuHostView.isHidden = true

            self.menuHostView.unsetActiveMenu()
            completion(success as AnyObject)
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let _keyPath: String = keyPath == nil ? "" : keyPath!

        switch _keyPath {
            case "orientationRawState":
                self.captureSessionManager.onLockUnLockOrientation((self.cameraSecondaryOptions?.orientationState)! as OrientationStates)
            case "gridRawState":
                switch (self.cameraSecondaryOptions?.gridState)! as GridFactor {
                case .off:
                    gridManager.gridFactor = .off
                case .double:
                    gridManager.gridFactor = .double
                case .quad:
                    gridManager.gridFactor = .quad
                }
            case "flashModeRawState":
                captureSessionManager.flashModeState = (cameraSecondaryOptions?.flashModeState)!
            case "hdrRawState":
                switch (self.cameraSecondaryOptions?.hdrState)! as HDREnum {
                case .on:
                    self.hdrFlag = true
                    self.hdrValueSetView.isHidden = false
                case .off:
                    self.hdrFlag = false
                    self.hdrValueSetView.isHidden = true
                }
            case "shakeRawState":
                self.view.isUserInteractionEnabled = false
                self.startDeviceMotion()
            case "tiltedRawState":
                switch (self.cameraSecondaryOptions?.tiltedState)! as TiltedEnum {
                case .on:
                    layerViewForPC.frame = self.captureSessionManager.previewLayer!.frame
                    self.innerLayerViewForCropping.isHidden = false
                    self.layerViewForPC.isHidden = false
                    self.deviceISTiltedOrNot()
                case .off:
//                    for vie in myCamView.subviews {
//                        if vie.tag == 10 {
//                            vie.removeFromSuperview()
//                        }
//                    }
//                    }
                    self.deviceTiltedStopped()
                    self.layerViewForPC.isHidden = true
                    //myCamView.layer.addSublayer((self.captureSessionManager.previewLayer)!)
                    //self.makingPhotoView.addSubview(myCamView)
                    self.innerLayerViewForCropping.isHidden = true
                }
            default:
                break
        }

    }
    
    
    func deviceISTiltedOrNot() {
        let xywidthheight = 0
        
        //makingPhotoViewHeightCons.constant = customViewHeightCons.constant
        //makingPhotoViewwidthCons.constant = customViewwidthCons.constant
        self.makingPhotoView.clipsToBounds = true
        
        originX = (Int(self.layerViewForPC.frame.origin.x))+xywidthheight
        originY = (Int(self.layerViewForPC.frame.origin.y))+xywidthheight
        heightCV = (Int(self.layerViewForPC.frame.height))-xywidthheight
        widthCV = (Int(self.layerViewForPC.frame.width))-xywidthheight
        startDeviceMotionForTilted()
    }
    
    func deviceTiltedStopped() {
        
        makingPhotoViewHeightCons.constant = self.layerViewForPC.frame.height
        makingPhotoViewwidthCons.constant = self.layerViewForPC.frame.width
        
//        self.xValue = 0
//        self.yValue = 0
//        let xywidthheight = 0
//
//        originX = xywidthheight
//        originY = xywidthheight
//        heightCV = xywidthheight
//        widthCV = xywidthheight
//        createRectangle()
//        createRectanglePortrait()
        self.timerForTilted.invalidate()
        self.timerForTilted = nil
        self.makingPhotoView.clipsToBounds = false
        oldShapLayer.path = nil
        for vie in layerViewForPC.layer.sublayers ?? view.layer.sublayers! {
            if vie.name == "rectangleLayer" {
                vie.removeFromSuperlayer()
            }
        }
        
    }
    
    
}

class SharedBlurView: UIVisualEffectView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.masksToBounds    = true
        self.layer.cornerRadius     = 5
    }    
}

class SharedButtonView: UIButton {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        DispatchQueue.global(qos: .userInteractive).async {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseIn, animations: {
                    self.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
                    self.transform = CGAffineTransform.init(translationX: 2, y: 0)
                    self.alpha = 0.5
                })
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        DispatchQueue.global(qos: .userInteractive).async {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseIn, animations: {
                    self.transform = CGAffineTransform.init(scaleX: 1, y: 1)
                    self.transform = CGAffineTransform.init(translationX: -2, y: 0)
                    self.alpha = 1
                })
            }
        }
    }
}


extension FirstViewController{
    
    func startDeviceMotion() {
        if motionManager.isDeviceMotionAvailable {
            self.motionManager.deviceMotionUpdateInterval = 0.5
            //self.motionManager.showsDeviceMovementDisplay = true
            self.motionManager.startDeviceMotionUpdates(using: .xMagneticNorthZVertical)
            
            // Configure a timer to fetch the motion data.
            self.timer = Timer(fire: Date(), interval: (0.5), repeats: true,
                               block: { (timer) in
                                if let data = self.motionManager.deviceMotion {
                                    // Get the attitude relative to the magnetic north reference frame.
                                    let x = data.attitude.pitch
                                    //let y = data.attitude.roll
                                    //let z = data.attitude.yaw
                                    
                                    let doubleStr = String(format: "%.2f", x)
                                    
                                    if(doubleStr == self.motionValue){
                                        print("Stop motion")
                                        if(self.timer != nil){
                                            self.timer.invalidate()
                                            self.timer = nil
                                            self.view.isUserInteractionEnabled = true
                                        }
                                        self.cameraSecondaryOptions?.lblShake.textColor = UIColor.lightGray
                                        self.captureImage()
                                    }else{
                                        print("Start motion")
                                    }
                                    
                                    self.motionValue = doubleStr
                                }
            })
            // Add the timer to the current run loop.
            RunLoop.current.add(self.timer!, forMode: RunLoop.Mode.default)        }
    }
    
}


extension FirstViewController{
    func getCaptureImage(image: UIImage){
        var captureImage = image
        if(!self.innerLayerViewForCropping.isHidden){
        //let frame = self.view.safeAreaInsets
        let imageV = UIImageView()
        let cV = UIView()
        imageV.frame = CGRect(x: 0, y: 0, width: self.myCamView.frame.width, height: self.myCamView.frame.height)//self.view.frame
        cV.frame = CGRect(x: 0, y: 0, width: self.myCamView.frame.width, height: self.myCamView.frame.height)//self.view.frame
            imageV.contentMode = .scaleAspectFit
        imageV.image = image
        cV.addSubview(imageV)
        cV.tag = 10
        self.myCamView.addSubview(cV)
            captureImage = self.makingPhotoView.asImage() ?? UIImage()//UIImage.init(view: self.makingPhotoView)
            //captureImage = UIImage.init(view: self.makingPhotoView).rotate(radians: .pi * 2.5)
            //captureImage = UIImage.init(view: self.makingPhotoView).rotate(radians: .pi * 1.5)
        for vie in myCamView.subviews {
            if vie.tag == 10 {
                vie.removeFromSuperview()
            }
        }
        }
        
        let qualityOfImageValue = Double(self.tfQualityofImage.text ?? "0")?.roundTo(2) ?? 0
        
        if(self.hdrFlag){
            let perValue = Double(10/(qualityOfImageValue*10)).roundTo(2)
            if(perValue < 9){
                if let imageNew = captureImage.resized(withPercentage: CGFloat(perValue)){
                    captureImage = imageNew
                }
            }
        }
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ImagePreviewViewController") as! ImagePreviewViewController
        controller.image = captureImage
        self.present(controller, animated: true, completion: nil)
        //self.navigationController?.pushViewController(controller, animated: true)
    }
}


extension FirstViewController{
    func startDeviceMotionForTilted() {
            if motionManager.isDeviceMotionAvailable {
                //self.motionManager.deviceMotionUpdateInterval = 0.2
                self.motionManager.startAccelerometerUpdates()
                self.motionManager.startGyroUpdates()
                self.motionManager.startMagnetometerUpdates()
                self.motionManager.startDeviceMotionUpdates()

                //Configure a timer to fetch the motion data.
                self.timerForTilted = Timer(fire: Date(), interval: (1.0), repeats: true,
                                   block: { (timerForTilted) in
                                    
                                    UIView.animate(withDuration: 1.0) {
                                               self.view.layoutIfNeeded()
                                               self.view.superview?.layoutIfNeeded()
                                               self.makingPhotoView.layoutIfNeeded()
                                               self.innerLayerViewForCropping.layoutIfNeeded()
                                    
                                    if let data = self.motionManager.deviceMotion {
//                                    self.rotationAngle = atan2(data.gravity.x, data.gravity.y) - Double.pi
//                                    //print("rotation:::\(self.rotationAngle)")
//                                        if(Int(self.rotationAngle*10) != 0){
//                                            self.makingPhotoView.transform  = CGAffineTransform(rotationAngle: CGFloat(self.rotationAngle))
//                                        }else{
//                                            self.rotationAngle = 0
//                                            self.makingPhotoView.transform  = CGAffineTransform(rotationAngle: CGFloat(0))
//                                        }
//                
                                        
                                        //let x = data.attitude.pitch
                                        let y = data.attitude.roll
                                    
//                                      let degree = data.attitude.pitch * 180 / Double.pi
//                                      print("Degree:::\(degree)")
                                        
                                        self.update()
                                        
                                        print("self.xValue \(self.xValue)")

                                        if(self.orientationsPosition == "portrait"){
                                            //self.yValue = ((x-1.5) * 100)
                                            self.createRectanglePortrait()
                                            self.manageInnerFrameVertically()
                                        }else if(self.orientationsPosition == "portraitUpsideDown"){
                                            //self.yValue = ((x+1.5) * 100)
                                            self.createRectanglePortrait()
                                            self.manageInnerFrameVertically()
                                        }else if(self.orientationsPosition == "landscapeLeft"){
                                            if(((y-1.5) * 200) > -self.horizontalDegree && ((y-1.5) * 200) < self.horizontalDegree){
                                                self.xValue = ((y-1.5) * 200)
                                                self.createRectangle()
                                                self.makingPhotoView.clipsToBounds = true
                                                self.manageInnerFrameHorizontally()
                                            }else{
                                                if(((y-1.5) * 200) < -self.horizontalDegree){
                                                    self.xValue = -self.horizontalDegree
                                                }
                                                if(((y-1.5) * 200) > self.horizontalDegree){
                                                    self.xValue = self.horizontalDegree
                                                }
                                                self.createRectangle()
                                                self.makingPhotoView.clipsToBounds = true
                                                self.manageInnerFrameHorizontally()
                                            }
                                        }else if(self.orientationsPosition == "landscapeRight"){
                                            if(((y+1.5) * 200) > -self.horizontalDegree && ((y+1.5) * 200) < self.horizontalDegree){
                                                self.xValue = ((y+1.5) * 200)
                                                self.createRectangle()
                                                self.makingPhotoView.clipsToBounds = true
                                                self.manageInnerFrameHorizontally()
                                            }else{
                                                if(((y+1.5) * 200) < -self.horizontalDegree){
                                                    self.xValue = -self.horizontalDegree
                                                }
                                                if(((y+1.5) * 200) > self.horizontalDegree){
                                                    self.xValue = self.horizontalDegree
                                                }
                                                self.createRectangle()
                                                self.makingPhotoView.clipsToBounds = true
                                                self.manageInnerFrameHorizontally()
                                            }
                                        }
                                        //print("self.xValue==\(self.xValue)")
                                    }
                                        
                                        
                                        self.view.layoutIfNeeded()
                                        self.view.superview?.layoutIfNeeded()
                                        self.makingPhotoView.layoutIfNeeded()
                                        self.innerLayerViewForCropping.layoutIfNeeded()
                                                   
                                               }
                                        
                })

                // Add the timer to the current run loop.
                RunLoop.current.add(self.timerForTilted!, forMode: RunLoop.Mode.default)

            }
    }
    
    func manageInnerFrameVertically(){
        
        var x1 = 0, x2 = Int(self.layerViewForPC.frame.width), x3 = Int(self.layerViewForPC.frame.width), x4 = 0
        if(originX - Int(self.yValue) > 0){
            x1 = originX - Int(self.yValue)
        }
        
        if(widthCV + Int(self.yValue) < Int(self.layerViewForPC.frame.size.width)){
            x2 = widthCV + Int(self.yValue)
        }
        
        if(widthCV - Int(self.yValue) < Int(self.layerViewForPC.frame.size.width)){
            x3 = widthCV - Int(self.yValue)
        }

        if(originX + Int(self.yValue) > 0){
            x4 = originX + Int(self.yValue)
        }
        
        if(self.yValue > 0){
//            if(Int(self.rotationAngle*10) != 0){
//                self.customViewwidthCons.constant = CGFloat(self.portraitTopWidth)
//                self.customViewHeightCons.constant = CGFloat(self.portraitTopWidth)
//            }else{
                self.customViewwidthCons.constant = CGFloat(x3 - x4)
                self.customViewHeightCons.constant = CGFloat(self.layerViewForPC.frame.height) - (CGFloat(x4)*5)
            //}
        }else{
            //print("rotation:::\(Int(self.rotationAngle*10))")
//            if(Int(self.rotationAngle*10) != 0){
//                self.customViewwidthCons.constant = CGFloat(self.portraitTopWidth) //CGFloat(x2 - x1) - (CGFloat(x1/2) - CGFloat(self.rotationAngle*120))
//                self.customViewHeightCons.constant = CGFloat(self.portraitTopWidth) //CGFloat(x2 - x1) - (CGFloat(x1/2) - CGFloat(self.rotationAngle*120))
//            }else{
                self.customViewwidthCons.constant = CGFloat(x2 - x1)
                self.customViewHeightCons.constant = CGFloat(self.layerViewForPC.frame.height) - (CGFloat(x1)*5)
            //}
            
        }
        
        self.makingPhotoViewwidthCons.constant = self.customViewwidthCons.constant
        if(self.customViewHeightCons.constant < 200){
            self.makingPhotoViewHeightCons.constant = 200//self.customViewHeightCons.constant
        }else{
            self.makingPhotoViewHeightCons.constant = self.customViewHeightCons.constant
        }
        
    }
    
    func manageInnerFrameHorizontally(){
        
        var y1 = 0, y2 = 0, y3 = Int(self.layerViewForPC.frame.size.height), y4 = Int(self.layerViewForPC.frame.size.height)
                       if(originY + Int(self.xValue) > 0){
                           y1 = originY + Int(self.xValue)
                       }
                       
                       if(originY - Int(self.xValue) > 0){
                           y2 = originY - Int(self.xValue)
                       }
                       
                       if(heightCV + Int(self.xValue) < Int(self.layerViewForPC.frame.size.height)){
                           y3 = heightCV + Int(self.xValue)
                       }
                       
                       if(heightCV - Int(self.xValue) < Int(self.layerViewForPC.frame.size.height)){
                           y4 = heightCV - Int(self.xValue)
                       }
        
        if(self.xValue > 0){
            self.customViewwidthCons.constant = CGFloat(self.layerViewForPC.frame.width) - CGFloat(y1)
            self.customViewHeightCons.constant = CGFloat(y4 - y1)
        }else{
            self.customViewwidthCons.constant = CGFloat(self.layerViewForPC.frame.width) - CGFloat(y2)
            self.customViewHeightCons.constant = CGFloat(y3 - y2)
        }
        
        self.makingPhotoViewwidthCons.constant = self.customViewwidthCons.constant
        self.makingPhotoViewHeightCons.constant = self.customViewHeightCons.constant
            
        
    }
    
    
    func createRectangle() {
            
            for vie in layerViewForPC.layer.sublayers ?? view.layer.sublayers! {
                if vie.name == "rectangleLayer" {
                    vie.removeFromSuperlayer()
                }
            }
            
             do {
                
                var y1 = 0, y2 = 0, y3 = Int(self.layerViewForPC.frame.size.height), y4 = Int(self.layerViewForPC.frame.size.height)
                if(originY + Int(self.xValue) > 0){
                    y1 = originY + Int(self.xValue)
                }
                
                if(originY - Int(self.xValue) > 0){
                    y2 = originY - Int(self.xValue)
                }
                
                if(heightCV + Int(self.xValue) < Int(self.layerViewForPC.frame.size.height)){
                    y3 = heightCV + Int(self.xValue)
                }
                
                if(heightCV - Int(self.xValue) < Int(self.layerViewForPC.frame.size.height)){
                    y4 = heightCV - Int(self.xValue)
                }
                
                
                       let rectangleLayer = try getPathPayer(arrPathPoints: [
                           CGPoint(x: originX, y: y1),    //Top-Left
                           CGPoint(x: widthCV, y: y2),    //Top-Right
                           CGPoint(x: widthCV, y: y3),    //Bottom-Right
                           CGPoint(x: originX, y: y4)])   //Bottom-Left
                       rectangleLayer.name = "rectangleLayer"
                
                //let color1 = hexStringToUIColor(hex: "#D3D3D3")
                //layerViewForPC.backgroundColor = color1
                
                //layerViewForPC.layer.addSublayer((self.captureSessionManager.previewLayer)!)
                
                
                //let radius: CGFloat = layerViewForPC.frame.width
                
                
                        //layerViewForPC.layer.mask = rectangleLayer
                       layerViewForPC.layer.addSublayer(rectangleLayer)
                   } catch {
                       debugPrint(error)
                   }
        }
    
    
    func createRectanglePortrait() {
        
        for vie in layerViewForPC.layer.sublayers ?? view.layer.sublayers!{
            if vie.name == "rectangleLayer" {
                vie.removeFromSuperlayer()
            }
        }
        
         do {
            var x1 = 0, x2 = Int(self.layerViewForPC.frame.width), x3 = Int(self.layerViewForPC.frame.width), x4 = 0
            if(originX - Int(self.yValue) > 0){
                x1 = originX - Int(self.yValue)
            }
            
            if(widthCV + Int(self.yValue) < Int(self.layerViewForPC.frame.size.width)){
                x2 = widthCV + Int(self.yValue)
            }
            
            if(widthCV - Int(self.yValue) < Int(self.layerViewForPC.frame.size.width)){
                x3 = widthCV - Int(self.yValue)
            }

            if(originX + Int(self.yValue) > 0){
                x4 = originX + Int(self.yValue)
            }
           
               // self.portraitTopWidth = (x2 - x1) - 50 //- ((Int(self.portraitTopWidth) > 200) ? 200 : 200)
            //self.portraitTopWidth = (((x2 - x1) - 50 > 280) ? 280 : (x2 - x1) - 50)
                //print("portraitTopWidth:::\(Int(self.portraitTopWidth))")
            
            
                   let rectangleLayer = try getPathPayer(arrPathPoints: [
                       CGPoint(x: x1, y: originY),    //Top-Left
                       CGPoint(x: x2, y: originY),    //Top-Right
                       CGPoint(x: x3, y: heightCV),    //Bottom-Right
                       CGPoint(x: x4, y: heightCV)])   //Bottom-Left
                   rectangleLayer.name = "rectangleLayer"
            
                    //let color1 = hexStringToUIColor(hex: "#D3D3D3")
                    //layerViewForPC.backgroundColor = color1
            
            //layerViewForPC.layer.addSublayer((self.captureSessionManager.previewLayer)!)
                    
                   layerViewForPC.layer.addSublayer(rectangleLayer)
            
            
            
            
            
            //layerViewForPC.layer.mask = rectangleLayer
            
            
               } catch {
                   debugPrint(error)
               }
    }
    
    func getPathPayer(arrPathPoints:[CGPoint]) throws -> CAShapeLayer {
        enum PathError : Error{
            case moreThan2PointsNeeded
        }

        guard arrPathPoints.count > 2 else {
            throw PathError.moreThan2PointsNeeded
        }

        let lineColor = UIColor.clear
        let lineWidth: CGFloat = 1
        let path = UIBezierPath()
        let pathLayer = CAShapeLayer()

        for (index,pathPoint) in arrPathPoints.enumerated() {
            switch index {
            //First point
            case 0:
                path.move(to: pathPoint)

            //Last point
            case arrPathPoints.count - 1:
                path.addLine(to: pathPoint)
                path.close()

            //Middle Points
            default:
                path.addLine(to: pathPoint)
            }
        }
        
        let color1 = hexStringToUIColor(hex: "#D3D3D3")

        pathLayer.path = path.cgPath
        pathLayer.strokeColor = lineColor.cgColor
        pathLayer.lineWidth = lineWidth
        pathLayer.fillColor = color1.cgColor
    
        
        let pathAnim = CABasicAnimation(keyPath: "path")
        if(oldShapLayer.path == nil){
            pathAnim.fromValue = path.cgPath
            self.layerViewForPC.isHidden = true
        }else{
            pathAnim.fromValue = oldShapLayer.path
            self.layerViewForPC.isHidden = false
        }
        pathAnim.toValue = path.cgPath
        pathAnim.duration = 1.0
        //pathAnim.autoreverses = true
        //pathAnim.repeatCount = HUGE
        pathLayer.add(pathAnim, forKey: "pathAnimation")
        oldShapLayer = pathLayer
        
        //layerViewForPC.layer.addSublayer((self.captureSessionManager.previewLayer)!)
        
        //let radius: CGFloat = layerViewForPC.frame.width/2
//        let path2 = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.layerViewForPC.frame.size.width, height: self.layerViewForPC.frame.size.height), cornerRadius: 0)
//        let circlePath = UIBezierPath(roundedRect: CGRect(x: self.makingPhotoView.frame.origin.x, y: self.makingPhotoView.frame.origin.y-93, width: self.makingPhotoView.frame.width, height: self.makingPhotoView.frame.height), cornerRadius: 0)
//
//        rotate(path: circlePath, degree: CGFloat(rotationAngle))
//
//        path2.append(circlePath)
//        path2.usesEvenOddFillRule = true
//
//        let fillLayer = CAShapeLayer()
//        fillLayer.path = path2.cgPath
//        fillLayer.fillRule = .evenOdd
//        fillLayer.fillColor = view.backgroundColor?.cgColor
//        fillLayer.opacity = 0.5
//        fillLayer.name = "rectangleLayer"
//
//        //let pathAnim2 = CABasicAnimation(keyPath: "path")
//        if(overLayShape.path == nil){
//            pathAnim.fromValue = path2.cgPath
//        }else{
//            pathAnim.fromValue = overLayShape.path
//        }
//        pathAnim.toValue = path2.cgPath
//        pathAnim.duration = 1.0
//        fillLayer.add(pathAnim, forKey: "pathAnimation")
//        overLayShape = fillLayer
//
//        layerViewForPC.layer.addSublayer(fillLayer)
        
        
        return pathLayer
    }
    
    
    func rotate(path: UIBezierPath, degree: CGFloat) {
        let bounds: CGRect = path.cgPath.boundingBox
        let center = CGPoint(x: bounds.midX, y: bounds.midY)

        let radians = degree / 180.0 * .pi
        var transform: CGAffineTransform = .identity
        transform = transform.translatedBy(x: center.x, y: center.y)
        transform = transform.rotated(by: radians)
        transform = transform.translatedBy(x: -center.x, y: -center.y)
        path.apply(transform)
    }
    
    
    @objc func update() {
    if let acceleration = motionManager.accelerometerData?.acceleration {
        if acceleration.x >= 0.75 {
            self.orientationsPosition = "landscapeLeft"
        }
        else if acceleration.x <= -0.75 {
            self.orientationsPosition = "landscapeRight"
        }
        else if acceleration.y <= -0.75 {
            
            self.orientationsPosition = "portrait"
        }
        else if acceleration.y >= 0.75 {
            self.orientationsPosition = "portraitUpsideDown"
        }
        else {
            //Consider same as last time
            //return
        }
        
        
        if(self.orientationsPosition == "portrait"){
            if(((acceleration.z) * 200) > -100 && ((acceleration.z) * 200) < 100){
                self.yValue = ((acceleration.z) * 200)
                self.makingPhotoView.clipsToBounds = true
            }else if(((acceleration.z) * 200) < -100){
                self.yValue = -100
                self.makingPhotoView.clipsToBounds = true
            }else if(((acceleration.z) * 200) > 100){
                self.yValue = 100
                self.makingPhotoView.clipsToBounds = true
            }
        }else if(self.orientationsPosition == "portraitUpsideDown"){
            if(((acceleration.z) * 200) > -100 && ((acceleration.z) * 200) < 100){
                self.yValue = ((acceleration.z) * 200)
                self.makingPhotoView.clipsToBounds = true
            }else if(((acceleration.z) * 200) < -100){
                self.yValue = -100
                self.makingPhotoView.clipsToBounds = true
            }else if(((acceleration.z) * 200) > 100){
                self.yValue = 100
                self.makingPhotoView.clipsToBounds = true
            }
        }
        
       
    }
    }
    
    
}



extension FirstViewController{
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(0.3)
        )
    }
}

extension FirstViewController{
    func showCurrentISOValue(isoValue: String){
        self.lblISO.text = "ISO " + isoValue
    }
}


extension UIImage{
    
    func renderResizedImage (newWidth: CGFloat, newHeight: CGFloat) -> UIImage {
        //let scale = newWidth / self.size.width
        //let newHeight = self.size.height * scale
        let newSize = CGSize(width: newWidth, height: newHeight)

        let renderer = UIGraphicsImageRenderer(size: newSize)

        let image = renderer.image { (context) in
            self.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        }
        return image
    }
    
func rotate(radians: CGFloat) -> UIImage {
    let rotatedSize = CGRect(origin: .zero, size: size)
        .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
        .integral.size
    UIGraphicsBeginImageContext(rotatedSize)
    if let context = UIGraphicsGetCurrentContext() {
        let origin = CGPoint(x: rotatedSize.width / 2.0,
                             y: rotatedSize.height / 2.0)
        context.translateBy(x: origin.x, y: origin.y)
        context.rotate(by: radians)
        draw(in: CGRect(x: -origin.y, y: -origin.x,
                        width: size.width, height: size.height))
        let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return rotatedImage ?? self
    }

    return self
}
    
    convenience init(view: UIView) {
           UIGraphicsBeginImageContext(view.frame.size)
           view.layer.render(in:UIGraphicsGetCurrentContext()!)
           let image = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
           self.init(cgImage: image!.cgImage!)
       }

}


extension FirstViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        tfCommon = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let allowedCharacters = CharacterSet(charactersIn:"-0123456789.")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
    }
    
}


extension UIView {

    func asImage() -> UIImage? {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
            defer { UIGraphicsEndImageContext() }
            guard let currentContext = UIGraphicsGetCurrentContext() else {
                return nil
            }
            self.layer.render(in: currentContext)
            return UIGraphicsGetImageFromCurrentImageContext()
        }
    }
}


extension FirstViewController{
    func setHRDSlider(){
        exposureSlider1.actionBlock = {slider,newValue,finished in
            self.lblExposureValue1.text = String(format: "%.1f", newValue)
            self.tfISO.text = "\(newValue)"
        }
        exposureSlider2.actionBlock = {slider,newValue,finished in
            self.lblExposureValue2.text = String(format: "%.1f", newValue)
            self.tfApeture.text = "\(newValue)"
        }
        exposureSlider3.actionBlock = {slider,newValue,finished in
            self.lblExposureValue3.text = String(format: "%.1f", newValue)
            self.tfShutter.text = "\(newValue)"
        }
        exposureSlider4.actionBlock = {slider,newValue,finished in
            self.lblExposureValue4.text = String(format: "%.1f", newValue)
            self.tfExposureValue4.text = "\(newValue)"
        }
        imageQualitySlider5.actionBlock = {slider,newValue,finished in
            self.lblQualityofImage.text = String(format: "%.1f", newValue)
            self.tfQualityofImage.text = String(format: "%.1f", newValue)
        }
    }
}
